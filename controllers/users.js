//[SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const auth = require('../auth');

//[SECTION] Functionalities [Create]
	module.exports.registerUser = (data)=>{
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let mobil = data.mobileNo;
			let newUser = new User({
				firstName: fName,
				lastName: lName,
				email: email,
				password: bcrypt.hashSync(passW, 10),
				mobileNo: mobil
			});
			return newUser.save().then((user, err)=>
			{
				if (user) {
					return user;
				} else {
					return false;
				}
			})
	}
	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email: reqBody.email}).then(result =>{
			if (result.length > 0) {
				return 'Email Already Exists';
			} else {
				return 'Email is still available';
			};
		});
	};
	module.exports.loginUser = (reqBody) =>{
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result=>{
			if (result === null) {
				return false;
			} else {
				let passW = result.password; 
				const isMatched = bcrypt.compareSync(uPassW,passW);
				if (isMatched) {
					let data = result.toObject();
					return {access: auth.createAccessToken(data)};
				} else {
					return false;
				}
			};
		});
	};

	module.exports.enroll = async (data) =>{
			let id = data.userId;
			let course = data.courseId;

			let isUserUpdated = await User.findById(id).then(user=>{
				const foundCourse = user.enrollments.find(x=>x.courseId===course)
				if (!foundCourse) {
					user.enrollments.push({courseId: course});
					return user.save().then((saved, error)=>{
						if (error) {
							return false;
						} else {
							return true;
						}
					});

				} else {
					return false;
				}
			});
					
			let isCourseUpdated = await Course.findById(course).then(course => {
				const foundCourse = course.enrollees.find(x=>x.userId===id)
				if (!foundCourse) {
					course.enrollees.push({userId: id});
					return course.save().then((saved, err)=>{
						if (err) {
							return false;
						} else {
							return true;
						};
					});

				} else {
					return false;
				}
			});

			if (isUserUpdated && isCourseUpdated) {
				return 'Successfully Enrolled'; 
			} else {
				return 'Enrollment Failed, Check Courses to be enrolled and Go to Registrar!';
			};
	};

//[SECTION] Functionalities [Retrieve]
	module.exports.getProfile = (id)=>{
		return User.findById(id).then(result=>{
			return result;
		});
	};
//[SECTION] Functionalities [Update]
	module.exports.setAsAdmin = (userId)=>{
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err)=>{
			if (admin) {
				return true;
			} else {
				return 'Updates Failed to implement';
			}
		});
	};
	//Set User as Non-Admin
	module.exports.setAsNonAdmin = (userId) =>{
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err)=>{
			if (user) {
				return true; 
			} else {
				return 'Failed to update user';
			};
		});
	};