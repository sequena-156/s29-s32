//[SECTION] Dependencies and Modules
	const Course = require('../models/Course');
//[SECTION] Functionality [Create]
	module.exports.addCourse = (info)=>{
			let course = info.course
			let cName = course.name;
			let cDesc = course.description;
			let cCost = course.price;
			let newCourse = new Course({
				name: cName,
				description: cDesc,
				price: cCost
			})
			return newCourse.save().then((savedCourse, err)=>{
				if (savedCourse) {
					return savedCourse;
				} else {
					return false;
				}
			});
	 };

//[SECTION] Functionality [Retrieve]
	module.exports.getAllCourse= () => {
		return Course.find({}).then(result => {
			return result;
		});
	}
	module.exports.getAllActive = () =>{
		return Course.find({isActive: true}).then(result=>{
			return result;
		});
	}
	module.exports.getCourse = (id) => {
		return Course.findById(id).then((foundCourse, err)=>{
			if (foundCourse) {
				return foundCourse;
			} else {
				return 'Course Not Found!';
			};
		});
	};

//[SECTION] Functionality [Update]
	//Update Course Details
	module.exports.updateCourse = (course, details) => {
		let cName =	details.name;		
		let cDesc =	details.description;
		let cCost =	details.price;
		let updatedCourse = {	
			name: cName,
			description: cDesc,
			price: cCost
		};	
		let id = course.courseId;
		return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err)=>{
			if (courseUpdated) {
				return courseUpdated;
			} else {
				return 'Failed to Update Course';
			}
		});
	};
	//Archive Course
	module.exports.archiveCourse = (course)=>{
			let id = course.courseId;
			let updates = {
				isActive: false
			} 
		return Course.findByIdAndUpdate(id,updates).then((archived, err)=>{
			if (archived) {
				return 'Course archived';
			} else {
				return false;
			}
		});
	};
//[SECTION] Functionality [Delete]
	//Delete Course
	module.exports.deleteCourse = (courseId)=>{
		return Course.findById(courseId).then(course =>{
			if (course === null) {
				return 'No Resource Was Found!';
			} else {
				return course.remove().then((removedCourse, err)=>{
					if (err) {
						return 'Failed to Remove Course';
					} else {
						return 'Successfully Destroyed Data';
					}
				});
			};
		});
	};