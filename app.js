//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const courseRoutes = require('./routes/courses');
	const userRoutes = require('./routes/users');


//[SECTION] Environment Variables Setup
	dotenv.config();
	const port = process.env.PORT;
	const credentials = process.env.MONGO_URL;

//[SECTION] Server Setup
	const app = express();
	app.use(cors()); //to allow resource sharing to other projects.
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));

//[SECTION] Database Connect
	mongoose.connect(credentials);
	const db = mongoose.connection;
	db.once('open', ()=> console.log('Connected to Atlas'));
	
//[SECTION] Server Routes
	app.use('/courses',courseRoutes);	
	app.use('/users', userRoutes);

//[SECTION] Server Responses
	app.get('/',(req,res)=>{
		res.send(`Project Deployed Successfully`);
	});
	app.listen(port, () => {
   		console.log(`API is now online on port ${port}`);
	});
 	